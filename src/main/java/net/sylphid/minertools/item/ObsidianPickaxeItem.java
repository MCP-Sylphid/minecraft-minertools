
package net.sylphid.minertools.item;

import net.sylphid.minertools.MinertoolsModElements;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.Item;
import net.minecraft.item.IItemTier;
import net.minecraft.block.Blocks;

@MinertoolsModElements.ModElement.Tag
public class ObsidianPickaxeItem extends MinertoolsModElements.ModElement {
	@ObjectHolder("minertools:obsidian_pickaxe")
	public static final Item block = null;
	public ObsidianPickaxeItem(MinertoolsModElements instance) {
		super(instance, 30);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new PickaxeItem(new IItemTier() {
			public int getMaxUses() {
				return 4500;
			}

			public float getEfficiency() {
				return 12f;
			}

			public float getAttackDamage() {
				return 2f;
			}

			public int getHarvestLevel() {
				return 5;
			}

			public int getEnchantability() {
				return 15;
			}

			public Ingredient getRepairMaterial() {
				return Ingredient.fromStacks(new ItemStack(NetheriteBladeItem.block, (int) (1)), new ItemStack(Blocks.OBSIDIAN, (int) (1)),
						new ItemStack(NetheritePickaxePlusItem.block, (int) (1)));
			}
		}, 1, 2f, new Item.Properties().group(ItemGroup.TOOLS)) {
		}.setRegistryName("obsidian_pickaxe"));
	}
}
