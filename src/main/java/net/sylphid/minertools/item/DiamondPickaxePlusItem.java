
package net.sylphid.minertools.item;

import net.sylphid.minertools.MinertoolsModElements;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.Item;
import net.minecraft.item.IItemTier;

@MinertoolsModElements.ModElement.Tag
public class DiamondPickaxePlusItem extends MinertoolsModElements.ModElement {
	@ObjectHolder("minertools:diamond_pickaxe_plus")
	public static final Item block = null;
	public DiamondPickaxePlusItem(MinertoolsModElements instance) {
		super(instance, 20);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new PickaxeItem(new IItemTier() {
			public int getMaxUses() {
				return 1785;
			}

			public float getEfficiency() {
				return 10f;
			}

			public float getAttackDamage() {
				return 2f;
			}

			public int getHarvestLevel() {
				return 1;
			}

			public int getEnchantability() {
				return 2;
			}

			public Ingredient getRepairMaterial() {
				return Ingredient.fromStacks(new ItemStack(DiamondBladeItem.block, (int) (1)));
			}
		}, 1, 0f, new Item.Properties().group(ItemGroup.TOOLS)) {
		}.setRegistryName("diamond_pickaxe_plus"));
	}
}
