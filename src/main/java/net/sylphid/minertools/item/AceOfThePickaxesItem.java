
package net.sylphid.minertools.item;

import net.sylphid.minertools.MinertoolsModElements;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.Item;
import net.minecraft.item.IItemTier;

@MinertoolsModElements.ModElement.Tag
public class AceOfThePickaxesItem extends MinertoolsModElements.ModElement {
	@ObjectHolder("minertools:ace_of_the_pickaxes")
	public static final Item block = null;
	public AceOfThePickaxesItem(MinertoolsModElements instance) {
		super(instance, 32);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new PickaxeItem(new IItemTier() {
			public int getMaxUses() {
				return 2500;
			}

			public float getEfficiency() {
				return 20f;
			}

			public float getAttackDamage() {
				return 2f;
			}

			public int getHarvestLevel() {
				return 6;
			}

			public int getEnchantability() {
				return 20;
			}

			public Ingredient getRepairMaterial() {
				return Ingredient.fromStacks(new ItemStack(DiamondBladeItem.block, (int) (1)), new ItemStack(NetheriteBladeItem.block, (int) (1)),
						new ItemStack(ObsidianPickaxeItem.block, (int) (1)), new ItemStack(NetheritePickaxePlusItem.block, (int) (1)));
			}
		}, 1, 4f, new Item.Properties().group(ItemGroup.TOOLS)) {
		}.setRegistryName("ace_of_the_pickaxes"));
	}
}
