
package net.sylphid.minertools.item;

import net.sylphid.minertools.MinertoolsModElements;

import net.minecraftforge.registries.ObjectHolder;

import net.minecraft.item.crafting.Ingredient;
import net.minecraft.item.PickaxeItem;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.Item;
import net.minecraft.item.IItemTier;

@MinertoolsModElements.ModElement.Tag
public class NetheritePickaxePlusItem extends MinertoolsModElements.ModElement {
	@ObjectHolder("minertools:netherite_pickaxe_plus")
	public static final Item block = null;
	public NetheritePickaxePlusItem(MinertoolsModElements instance) {
		super(instance, 21);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new PickaxeItem(new IItemTier() {
			public int getMaxUses() {
				return 2570;
			}

			public float getEfficiency() {
				return 4f;
			}

			public float getAttackDamage() {
				return 2f;
			}

			public int getHarvestLevel() {
				return 1;
			}

			public int getEnchantability() {
				return 2;
			}

			public Ingredient getRepairMaterial() {
				return Ingredient.fromStacks(new ItemStack(NetheriteBladeItem.block, (int) (1)));
			}
		}, 1, 1f, new Item.Properties().group(ItemGroup.TOOLS)) {
		}.setRegistryName("netherite_pickaxe_plus"));
	}
}
